<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Search Function with MySQL</title>
</head>
<body>
  <h1>Search Function with MySQL</h1>
  
  <form method="GET">
    <input type="text" name="search" placeholder="Search...">
    <button type="submit">Search</button>
  </form>

  <?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "mahasiswa";

  $conn = new mysqli($servername, $username, $password, $dbname);

  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  if (isset($_GET['search'])) {
      $searchTerm = $_GET['search'];

      $sql = "SELECT * FROM student WHERE 
              nama LIKE '%$searchTerm%' OR 
              nim LIKE '%$searchTerm%' OR 
              jurusan LIKE '%$searchTerm%' OR 
              kelompok LIKE '%$searchTerm%' OR 
              grup LIKE '%$searchTerm%' OR 
              pembimbing LIKE '%$searchTerm%'";
              
      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
          echo "<h2>Search Results:</h2>";
          echo "<table>";
          echo "<tr><th>Nama</th><th>NIM</th><th>Jurusan</th><th>Kelompok</th><th>Grup</th><th>Pembimbing</th></tr>";
          
          while ($row = $result->fetch_assoc()) {
              echo "<tr>";
              echo "<td>" . $row['nama'] . "</td>";
              echo "<td>" . $row['nim'] . "</td>";
              echo "<td>" . $row['jurusan'] . "</td>";
              echo "<td>" . $row['kelompok'] . "</td>";
              echo "<td>" . $row['grup'] . "</td>";
              echo "<td>" . $row['pembimbing'] . "</td>";
              echo "</tr>";
          }
          echo "</table>";
      } else {
          echo "<p>No results found.</p>";
      }
  }
  
  $conn->close();
  ?>
  
</body>
</html>
