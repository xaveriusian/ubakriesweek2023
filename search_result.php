<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kelompok UBakries Week 2023</title>
<style>
        body{
            background-color: #2B9DB2;
            margin:0;
            font-family: 'Poppins', sans-serif;
        }
        .search-bg{
            object-fit: cover;
            height: 100vh;
            width: 60%;
           
            
            
            background-attachment:fixed;
        }
        .title-search{
            margin-left:4% ;
            margin-bottom: 1px;
            margin-top: -38%;
        }
        /* .search-form{
            width: 20%;
        } */

        .search-form input{
            border : none !important;
            outline:none! important;
            padding-left: 93px;
            padding-bottom: 10px;
            border-radius: 11px;
        }
        ::placeholder {
            font-style: italic;
            font-size: 1em;
            color: mintcream;
            
        }
        table{
            text-align: center;
        }
        .result{
            margin-bottom:20%;
            font-size :13px;
            text-align:left;
            font-weight:600;
        }
        .result-container{
            margin-left:-41%;
            width :31%;
            margin-top:6%;
            font-weight:600;
        }
        .result button{
            width :74%;
            background-color:#fff;
            border :none;
            padding :15px;
            border-radius:10px;
            margin-top:-10%;
            font-weight:600;
            text-align:left;
        }
        .icon-back{
            color: #000;
            background-color:#fff;
            padding:15px;
            border-radius:10px;
            width:20%;
            justify-content:center;
            text-align:center;
            margin-top:-56px;
            margin-left:-79px;
            margin-bottom:33px;
        }
    </style>
     <link href="css/style.css" rel="stylesheet">
     <script src="https://kit.fontawesome.com/11dd8dbdc4.js" crossorigin="anonymous"></script>
     <link rel="preconnect" href="https://fonts.googleapis.com">
     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;1,500;1,600&display=swap" rel="stylesheet">
</head>
<body>
<div style="display:flex;flex-direction:row-reverse;height:100vh;">
<img class="search-bg" src="img/2023/Group4.png">
  <!-- <h1>Search Results</h1> -->
  <?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "mahasiswa";

  $conn = new mysqli($servername, $username, $password, $dbname);

  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  if (isset($_GET['search'])) {
      $searchTerm = $_GET['search'];

      $sql = "SELECT * FROM student WHERE 
              nama LIKE '%$searchTerm%' OR 
              nim LIKE '%$searchTerm%' OR 
              jurusan LIKE '%$searchTerm%' OR 
              kelompok LIKE '%$searchTerm%' OR 
              grup LIKE '%$searchTerm%' OR 
              pembimbing LIKE '%$searchTerm%'";
              
      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
       
        echo "<div class='result-container'>";
        //   echo "<h2>Search Results:</h2>";
        echo "<a href='search.php'><i class='fa-solid fa-arrow-left icon-back'></i></a>";
          echo "<div class='result'>";
          while ($row = $result->fetch_assoc()) {
              echo "<div class='result-php'>";
              echo "<h1> Halo,  <u>" . $row['nama'] . "</u>!</h1>";
              
              echo "</div>";
              echo "<tr>";
              
              echo "<h1>Kelompok</h1>";
              echo "<button>" . $row['kelompok'] . "</button><br>";
              echo "<h1>Pembimbing Kelompok</h1>";
              echo "<button>" . $row['pembimbing'] . "</button>";
              echo "<h1>Group Line</h1>";
              echo "<button> <a href=".$row['grup']."> " . $row['grup'] . "</a></button>";
              echo "</tr>";
          }
          echo "</div>";
         
          
      } else {
          
          echo "<p>No results found.</p>";
          echo "<a href='search.php'><i class='fa-solid fa-arrow-left'></i></a>";
      }
  }
  echo "</div>";
  $conn->close();
  ?>
  </div>
</body>
</html>
