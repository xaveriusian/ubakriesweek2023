<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cari Kelompokmu!</title>
    <style>
        body{
            background-color: #2B9DB2;
        }
        .search-bg{
            background-size: cover;
            height: fit-content;
            width: 70%;
            margin-left: 36%;
            margin-bottom: px;
            margin-top: -18px;
            background-attachment:fixed;
        }
        .title-search{
            margin-left:4% ;
            margin-bottom: 1px;
            margin-top: -38%;
        }
        /* .search-form{
            width: 20%;
        } */

        .search-form input{
            border : none !important;
            outline:none! important;
            padding-left: 93px;
            padding-bottom: 10px;
            border-radius: 11px;
        }
        ::placeholder {
            font-style: italic;
            font-size: 1em;
            color: mintcream;
            
        }
        form{
            width :50%;
        }
        .sera input{
            padding:10px;
            width :40%;
            border-radius :19px;
        }
        .icon-search{
            background-color:none;
        }
        .sera button{
            background-color:#fff;
            font-size:25px;
            border-radius:150px;
            border:none;
            margin-top:5px;
            padding:5px
        }
    </style>
    <script src="https://kit.fontawesome.com/11dd8dbdc4.js" crossorigin="anonymous"></script>
</head>
<body>
    <img class="search-bg" src="img/2023/Group4.png">
    <div class="title-search">
        <h1>NIM</h1>
        <div class="sera">
            <form method="GET" action="search_result.php">
                <input type="text" name="search" placeholder="Search...">
           
                <button type="submit" ><i class="fa-solid fa-magnifying-glass icon-search"></i></button>
            
            </form>
        </div>
    </div>
    <?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "mahasiswa";

  $conn = new mysqli($servername, $username, $password, $dbname);

  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  if (isset($_GET['search'])) {
      $searchTerm = $_GET['search'];

      $sql = "SELECT * FROM student WHERE 
              nama LIKE '%$searchTerm%' OR 
              nim LIKE '%$searchTerm%' OR 
              jurusan LIKE '%$searchTerm%' OR 
              kelompok LIKE '%$searchTerm%' OR 
              grup LIKE '%$searchTerm%' OR 
              pembimbing LIKE '%$searchTerm%'";
              
      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
          echo"";
          echo "<h2>Search Results:</h2>";
          echo "<table>";
          echo "<tr><th>Nama</th><th>NIM</th><th>Jurusan</th><th>Kelompok</th><th>Grup</th><th>Pembimbing</th></tr>";
          
          while ($row = $result->fetch_assoc()) {
              echo "<tr>";
              echo "<td>" . $row['nama'] . "</td>";
              echo "<td>" . $row['nim'] . "</td>";
              echo "<td>" . $row['jurusan'] . "</td>";
              echo "<td>" . $row['kelompok'] . "</td>";
              echo "<td>" . $row['grup'] . "</td>";
              echo "<td>" . $row['pembimbing'] . "</td>";
              echo "</tr>";
          }
          echo "</table>";
      } else {
          echo "<p>No results found.</p>";
      }
  }
  
  $conn->close();
  ?>
</body>
</html>